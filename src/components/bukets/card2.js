import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
import leaf from "../../assets/Group 100.png";
import person from "../../assets/Group 101.png";
import truck from "../../assets/Group 166.png";
import flip from "../../assets/Group 99.png";
const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    marginTop: 9,
    display: "block",
    backgroundColor: "white",
    textAlign: "left",
  },
  color: {
    color: "white",
  },
  buket: {
    marginLeft: 10,
  },
  tableimg: {
    backgroundColor: "#F0F0F0",
    borderRadius: "50%",
    padding: 5,
  },
  tabletext: {
    color: "grey",
    textTransform: "capitalize",
  },
  tabletext1: {
    textTransform: "capitalize",
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      color: "#535353",
    },
    subtitle1: {
      fontSize: 13,
      fontWeight: "bold",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 10,
      fontWeight: "bold",
      textTransform: "lowercase",
    },
  },
});

export default function Cards() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={CardTheme}>
      <Box boxShadow={2} className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Box p={1}>
              <p>Transaction history</p>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <TableContainer component={Paper}>
              <Table
                className={classes.table}
                size="small"
                aria-label="a dense table"
              >
                <TableHead>
                  <TableRow>
                    <TableCell>
                      {" "}
                      <span className={classes.tabletext}>Reciever</span>
                    </TableCell>
                    <TableCell>
                      <span className={classes.tabletext}>Date</span>
                    </TableCell>
                    <TableCell>
                      <span className={classes.tabletext}>Time</span>
                    </TableCell>
                    <TableCell>
                      <span className={classes.tabletext}>Amount</span>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <Box display="flex">
                        <Box className={classes.tableimg}>
                          <img src={flip} />
                        </Box>
                        <Box style={{ marginTop: 6 }}>
                          <span className={classes.tabletext1}>
                            &nbsp;&nbsp;&nbsp;Tesco market
                          </span>
                        </Box>
                      </Box>
                    </TableCell>
                    <TableCell align="left">
                      <span className={classes.tabletext}>shopping</span>
                    </TableCell>

                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext}>13 Dec 2020</span>
                    </TableCell>
                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext1}>$75.67</span>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Box display="flex">
                        <Box className={classes.tableimg}>
                          <img src={truck} />
                        </Box>
                        <Box style={{ marginTop: 6 }}>
                          <span className={classes.tabletext1}>
                            &nbsp;&nbsp;&nbsp;ElectroMan market
                          </span>
                        </Box>
                      </Box>
                    </TableCell>
                    <TableCell align="left">
                      <span className={classes.tabletext}>shopping</span>
                    </TableCell>

                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext}>14 Dec 2020</span>
                    </TableCell>
                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext1}>$250.0</span>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Box display="flex">
                        <Box className={classes.tableimg}>
                          <img src={leaf} />
                        </Box>
                        <Box style={{ marginTop: 6 }}>
                          <span className={classes.tabletext1}>
                            &nbsp;&nbsp;&nbsp;Fiorgio Restourant
                          </span>
                        </Box>
                      </Box>
                    </TableCell>
                    <TableCell align="left">
                      <span className={classes.tabletext}>shopping</span>
                    </TableCell>

                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext}>7 Sec 2020</span>
                    </TableCell>
                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext1}>$19.50</span>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Box display="flex">
                        <Box className={classes.tableimg}>
                          <img src={person} />
                        </Box>
                        <Box style={{ marginTop: 6 }}>
                          <span className={classes.tabletext1}>
                            &nbsp;&nbsp;&nbsp;Jhon Mathew kayne
                          </span>
                        </Box>
                      </Box>
                    </TableCell>
                    <TableCell align="left">
                      <span className={classes.tabletext}>shopping</span>
                    </TableCell>

                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext}>6 Dec 2020</span>
                    </TableCell>
                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext1}>$350</span>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Box display="flex">
                        <Box className={classes.tableimg}>
                          <img src={person} />
                        </Box>
                        <Box style={{ marginTop: 6 }}>
                          <span className={classes.tabletext1}>
                            &nbsp;&nbsp;&nbsp;Ann Marlin
                          </span>
                        </Box>
                      </Box>
                    </TableCell>
                    <TableCell align="left">
                      <span className={classes.tabletext}>shopping</span>
                    </TableCell>

                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext}>31 Nov 2020</span>
                    </TableCell>
                    <TableCell align="left">
                      {" "}
                      <span className={classes.tabletext1}>$430</span>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
      </Box>
    </ThemeProvider>
  );
}
