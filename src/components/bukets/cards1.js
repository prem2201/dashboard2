import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
import arrowright from "../../assets/Vector 39.png";
import arrowleft from "../../assets/Vector 40.png";
import Progress from "./progress";
const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
    marginTop: 9,
    display: "block",
    backgroundColor: "white",
    textAlign: "left",
  },
  color: {
    color: "white",
  },
  buket: {
    marginLeft: 10,
  },
  icon: {
    fontSize: 13,
    color: "#E5513C",
  },
  icons: {
    color: "black",
    fontSize: 24,
    marginTop: 3,
  },
  border: {
    border: "1px solid #E4EAEE",
    padding: 6,
    borderRadius: 8,
  },
  box: {
    width: "90%",
    padding: 15,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 18,
    paddingLeft: 18,
  },
  debit: {
    backgroundColor: "#2981cc",
    paddingLeft: 20,
    borderRadius: 20,
    height: 200,
  },
  sideborder: {
    borderLeft: "1px solid #666666",
  },
  left: {
    float: "left",
    marginLeft: 14,
  },
  hello: {
    backgroundColor: "rgb(243, 10, 10)",
    boxShadow: "none",
  },
  sub: {
    color: "#E4513E",
  },
  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
    marginLeft: 7,
  },
  btn: {
    border: "1px solid #E4EAEE",
    float: "right",
  },

  progress: {
    color: "#2B81CC",
    marginTop: 3,
    marginLeft: 3,

    width: 20,
  },
  cardcontent: {
    marginLeft: 8,
    padding: 5,
  },
  cardicon: {
    color: "#949494",
    fontSize: 16,
    marginTop: 4,
  },

  btnicon: {
    fontSize: 4,
  },

  typo: {
    marginTop: -10,
  },
  subtitle3: {
    fontSize: 10,
    float: "right",
    display: "block",
  },
  disable: {
    float: "right",
    color: "grey",
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      color: "white",
    },

    subtitle1: {
      fontSize: 15,
      fontWeight: "600",
      fontFamily: "adobe-clean, sans-serif",
      color: "white",
    },
    body2: {
      fontSize: 12,
      padding: 1,
      textTransform: "lowercase",
    },
    h1: {
      fontSize: 26,
      textTransform: "lowercase",
      textAlign: "right",
    },
    h2: {
      fontSize: 20,
      textAlign: "right",
      textTransform: "lowercase",
    },
    h3: {
      fontSize: 17,
      textAlign: "right",
      textTransform: "lowercase",
    },
    h5: {
      color: "white",
      fontSize: 20,
      marginTop: 35,
      textAlign: "center",
      wordSpacing: 10,
    },
  },
});

export default function Cards() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
  });
  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <ThemeProvider theme={CardTheme}>
      <Box className={classes.root} boxShadow={2}>
        <Grid container>
          <Grid item xs={12}>
            <p className={classes.left}>Cards</p>
          </Grid>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={9}>
                <div style={{ width: "100%" }}>
                  <Box display="flex" flexDirection="row" p={1} m={1}>
                    <Box p={1} alignSelf="center">
                      <img src={arrowleft} alt="" />
                    </Box>
                    <Box p={1} flexGrow={1} className={classes.debit}>
                      <Typography variant="subtitle1">Cloud cash</Typography>
                      <Typography variant="subtitle2">
                        PREMIUM ACCOUNT
                      </Typography>
                      <Typography variant="h5">5789 **** **** 2789</Typography>
                      <Grid container>
                        <Grid item sm={5}>
                          <br />
                          <Typography variant="subtitle2" m={2}>
                            Card holder
                          </Typography>
                          <Typography variant="subtitle1">
                            Mike Smith
                          </Typography>
                        </Grid>
                        <Grid item sm={5}>
                          <br />
                          <Typography variant="subtitle2" m={2}>
                            Expire date
                          </Typography>
                          <Typography variant="subtitle1">16/21</Typography>
                        </Grid>
                      </Grid>
                    </Box>
                    <Box p={1} alignSelf="center">
                      <img src={arrowright} alt="" />
                    </Box>
                  </Box>
                </div>
                <Grid container>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <Progress />
                    </Box>
                  </Grid>
                  <Grid item xs={6}>
                    <Box>
                      <Typography>Cloud cash</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={6}>
                    <Box>
                      <Typography>$350.60/$4000.00</Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <Grid container>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <Typography variant="h1">$2850.75</Typography>
                      <Typography className={classes.subtitle3}>
                        Current balance
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <br />
                      <Typography variant="h2">$1500.50</Typography>
                      <Typography className={classes.subtitle3}>
                        Income
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <br />
                      <Typography variant="h3">$350.60</Typography>
                      <Typography className={classes.subtitle3}>
                        Outcome
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <br />
                      <div style={{ float: "right" }}>
                        <Switch
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </div>
                      <p className={classes.disable}>Deactive Card</p>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </ThemeProvider>
  );
}
