import Grid from "@material-ui/core/Grid";
import React from "react";
import { SideNavBar } from "./sideNavbar";
class SideNavbarParent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Grid container>
          <Grid item xs={3}>
            <SideNavBar />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default SideNavbarParent;
