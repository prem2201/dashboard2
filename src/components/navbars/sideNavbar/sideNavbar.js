import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { BiUpArrowAlt } from "react-icons/bi";
import { BsCloudDownload } from "react-icons/bs";
import goal from "../../../assets/award.svg";
import graph from "../../../assets/Group 70.png";
import transcation from "../../../assets/Group 73.svg";
import card from "../../../assets/Group 74.svg";
import invoice from "../../../assets/Group 75.svg";
import settings from "../../../assets/Group 76.svg";
import cloud from "../../../assets/Vector.png";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    position: "absolute",
    zIndex: 1,
    height: "100%",
  },
  drawer: {
    width: "21%",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
  icons: {
    fontSize: 23,
    color: "white",
    fontWeight: "bold",
  },
  topicon: {
    fontSize: 50,
    color: "#2C7BBD",
    fontWeight: "bold",
    textAlign: "center",
  },
  bgicons: {
    backgroundColor: "#333069",
    borderRadius: 8,
    marginTop: 10,
    padding: 5,
  },
  button: {
    marginTop: 14,
    backgroundColor: "#efc714",
    borderRadius: 14,
  },
  bottom: {
    marginTop: 163,
  },
  text: {
    color: "#1670c9",
  },
  text1: {
    color: "grey",
  },
  text2: {
    color: "violate",
  },
  bg: {
    backgroundColor: "#dcebfa",
    marginTop: 30,
  },
  drawerContainer: {
    padding: 40,
  },
  top: {
    marginTop: "70%",
  },
}));

export const SideNavBar = (props) => {
  const classes = useStyles(props);

  const outerTheme = createMuiTheme({
    palette: {
      secondary: {
        main: "#fff",
      },
    },
    typography: {
      subtitle2: {
        fontSize: 12,
        textAlign: "center",
        color: "grey",
      },
      subtitle1: {
        fontSize: 12,
        textAlign: "center",
        textTransform: "capitalize",
        padding: 7,
      },
    },
  });
  return (
    <ThemeProvider theme={outerTheme}>
      <div className={classes.root}>
        <Paper className={classes.drawer} square>
          <div className={classes.drawerContainer}>
            <List>
              <ListItem button>
                <ListItemIcon>
                  <div>
                    <img src={cloud} alt="" />
                  </div>
                </ListItemIcon>
                <ListItemText primary="Cloudcash" className={classes.text2} />
              </ListItem>
            </List>
            <List>
              <ListItem button className={classes.bg}>
                <ListItemIcon>
                  <div className={classes.bgicon}>
                    <img src={graph} alt="" />
                  </div>
                </ListItemIcon>
                <ListItemText primary="Overview" className={classes.text} />
              </ListItem>

              {
                <ListItem button>
                  <ListItemIcon>
                    <img src={transcation} alt="" n />
                  </ListItemIcon>
                  <ListItemText
                    primary="Transcation"
                    className={classes.text1}
                  />
                </ListItem>
              }

              {
                <ListItem button>
                  <ListItemIcon>
                    <img src={card} alt="" />
                  </ListItemIcon>
                  <ListItemText primary="Cards" className={classes.text1} />
                </ListItem>
              }

              {
                <ListItem button>
                  <ListItemIcon>
                    <img src={invoice} alt="" />
                  </ListItemIcon>
                  <ListItemText primary="Invoice" className={classes.text1} />
                </ListItem>
              }

              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgiconss}>
                      <img src={goal} alt="" />
                    </div>
                  </ListItemIcon>
                  <ListItemText primary="Goals" className={classes.text1} />
                </ListItem>
              }
              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgiconss}>
                      <img src={settings} alt="" />
                    </div>
                  </ListItemIcon>
                  <ListItemText primary="Settings" className={classes.text1} />
                </ListItem>
              }
            </List>{" "}
            <div button className={classes.top}>
              <center>
                <BsCloudDownload className={classes.topicon} />
                <Typography variant="subtitle2">give your money</Typography>
                <Typography variant="subtitle2">
                  Awaseme space in cloud
                </Typography>
                <Button className={classes.button} endIcon={<BiUpArrowAlt />}>
                  <Typography variant="subtitle1">
                    Upgrade to premium
                  </Typography>
                </Button>
              </center>
            </div>
          </div>
        </Paper>
      </div>
    </ThemeProvider>
  );
};
