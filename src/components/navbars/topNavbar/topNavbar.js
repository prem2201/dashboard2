import { Drawer } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import { SideNavBar } from "..";
import note from "../../../assets/bell.png";
import profile from "../../../assets/Group 135.png";
import mail from "../../../assets/mail.png";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "white",
  },
  title: {
    color: "black",
    marginTop: 14,
    float: "left",
  },

  menuIcon: {
    marginLeft: -13,
    marginTop: 13,
  },
  search: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
  },
  inputs: {
    padding: 3,
  },
  circle: {
    width: "1.6rem",
    height: "1.6rem",
    padding: 5,
    marginTop: 10,
    marginRight: 13,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    padding: 8,
    textTransform: "capitalize",
    fontSize: 14,
  },
  profile: {
    width: "3rem",
    height: "3rem",
    borderRadius: 16,
    marginTop: 7,
    marginLeft: 10,
  },
  icons: {
    color: "black",
    fontSize: 25,
    marginTop: 6,
  },
  icon: {
    fontSize: 10,
  },
  note: {
    color: "#4B9CB4",
    fontSize: 12,
    position: "absolute",
    top: 7,
    left: 15,
    backgroundColor: "white",
  },
  border: {
    borderLeft: "1px solid #E4EAEE",
    padding: 5,
    height: 25,
    marginTop: 9,
  },
  heading: {
    fontSize: 37,
    color: "black",
  },
}));
const defaultProps = {
  bgcolor: "#EFC715",
  borderColor: "#EFC715",
  border: 1,
  style: { width: "2.3rem", height: "2.3rem" },
  marginTop: 2,
  marginRight: 2,
};
const TopbarTheme = createMuiTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      textAlign: "center",
    },
    h6: {
      fontSize: 14,
      color: "grey",
      fontWeight: 400,
    },
    h4: {
      fontSize: 37,
      paddingBottom: 10,
    },
  },
});

export const TopNavBar = (props) => {
  const classes = useStyles();

  const [state, setState] = React.useState({
    openSideNavBar: false,
  });

  const toogleSideNavBar = () => {
    setState({
      ...state,
      openSideNavBar: !state.openSideNavBar,
    });
  };

  return (
    <ThemeProvider theme={TopbarTheme}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Grid container style={{ marginTop: 10 }}>
              <Grid item xs={1}>
                <IconButton
                  className={classes.menuIcon}
                  onClick={toogleSideNavBar}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
              <Grid item xs={11}>
                <Grid container>
                  <Grid item xs={12} md={4} lg={6}>
                    <span className={classes.heading}> Weekly sumup</span>
                    <br />
                    <span style={{ color: "grey" }}>
                      Get Summery of Your Weekly Online Transcation here
                    </span>
                    <br />
                    <br />
                  </Grid>
                  <Grid item xs={12} md={3} lg={1}></Grid>
                  <Grid item xs={12} md={5} lg={5}>
                    <div style={{ width: "100%" }}>
                      <Box
                        m={1}
                        display="flex"
                        flexDirection="row"
                        justifyContent="flex-end"
                      >
                        <Box m={2} marginRight={-0}>
                          <img src={mail} alt="" />
                        </Box>

                        <Box m={2}>
                          <IconButton size="small">
                            <img src={note} alt="" />
                          </IconButton>
                        </Box>

                        <Box borderRadius="50%" {...defaultProps}>
                          <img src={profile} alt="" />
                        </Box>
                        <Box mt={2}>
                          <span style={{ color: "black" }}>Andrew</span>
                          <Typography variant="h6">Admin account</Typography>
                        </Box>
                      </Box>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Drawer
              open={state.openSideNavBar}
              variant={"temporary"}
              anchor="left"
              onClose={toogleSideNavBar}
            >
              <div style={{ width: "290px" }}>
                <SideNavBar isMobile={true} />
              </div>
            </Drawer>
          </Toolbar>
        </AppBar>
      </div>
    </ThemeProvider>
  );
};
