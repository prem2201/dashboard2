import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import React from "react";
import Bukect from "../../components/buckets2/index";
import Buket from "../../components/bukets/buket";
const useStyles = makeStyles((theme) => ({
  paper: {
    borderRadius: 10,
  },
  rightbar: {
    marginTop: -10,
  },
}));

export const Home = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={12} md={12} lg={6}>
          <Box width="100%">
            <Buket />
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={6}>
          <Box m={1}>
            <Bukect />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};
